import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 4.7 Inverteix l’array
*/


fun main(args: Array<String>) {
 var argsReverse=args
 for (i in 0..args.lastIndex){    /*este bucle recorre la POSICION de la array(es lo mismo de la condicion (i in args.indices)   indice = posocion del array*/
     argsReverse[i] = args[args.lastIndex-i]
 }
    for (element in argsReverse) println(element)

}