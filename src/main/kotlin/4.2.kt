/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 4.2 Calcula la mitjana
*/

fun main(args: Array<String>) {

    var result = 0
    for (i in args){
        val suma=i.toInt()
        result+=suma
    }

    val media = result.toDouble()/args.size

    println(media)
}