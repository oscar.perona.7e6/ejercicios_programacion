import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 3.10 Extrems
*/

fun main() {

    val scanner = Scanner(System.`in`)
    println("Introdueix un valor:")
    var entrada: Int = scanner.nextInt()

    var mayor=0
    var menor=0

    while (entrada!=0) {
        val scanner = Scanner(System.`in`)
        println("Introdueix un valor:")
        var entrada: Int = scanner.nextInt()

        if (entrada > mayor && entrada != 0) mayor = entrada
        else if (entrada < menor && entrada != 0) menor = entrada
        else if (entrada == 0) break
    }

    println("Nombre mayor $mayor Nombre menor $menor")
}