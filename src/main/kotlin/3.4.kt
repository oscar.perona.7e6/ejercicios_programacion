import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 3.4 Imprimeix el rang2
*/

fun main(){

    val scanner = Scanner(System.`in`)
    println("Introdueix un valor:")
    var number1: Int = scanner.nextInt()
    val number2: Int = scanner.nextInt()


    for (i in number1..number2) print("i"+",")
    for (i in number2..number1) print("i"+",")

}