import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/11/09
* TITLE: 4.31 Comptant a’s
*/


fun main(){

    var numberOfA = 0
    val scanner = Scanner(System.`in`)
    println("escribe una frase")
    val frase = scanner.nextLine()

    for (i in frase){
        if (i == '.') break
        else if (i == 'a' || i == 'A') numberOfA++
    }

    println(numberOfA)
}