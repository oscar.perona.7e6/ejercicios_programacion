import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 3.20 Triangle de nombres
*/

fun main(){

    val scanner = Scanner(System.`in`)
    println("Introdueix una direccio:")
    var lines = scanner.nextInt()

    for (i in 1..lines){
            for (number in 1..i){
            print(number)
            }

        println()
    }
}