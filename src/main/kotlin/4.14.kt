import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/11/09
* TITLE: 4.14 Quants sumen...?
*/

fun main(args: Array<String>){

    val scanner = Scanner(System.`in`)
    println("Introdueix un valor:")
    val n = scanner.nextInt()



    for (i in args){
        for (j in args){
            if (i.toInt() + j.toInt() == n) println("$i  $j")
        }
    }
}