/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 4.9 Valors repetits
*/


fun main(args: Array<String>) {

    for(i in 0..args.lastIndex){
        for (j in i+1 .. args.lastIndex){
            if (args[i] == args[j]) println(args[i])
        }
    }
}