import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/11/09
* TITLE: 4.20 Distància d'Hamming
*/

fun main(){

    val scanner = Scanner(System.`in`)
    println("Introdueix dos ADN")
    val adn1:String = scanner.next()
    val adn2:String = scanner.next()
    var counter = 0

    if (adn1.length == adn2.length){
        for (i in adn1.indices){
            if (adn1[i] != adn2[i]) counter++
        }
        println(counter)
    }
    else println("Entrada no valida")
}