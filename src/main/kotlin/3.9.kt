import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 3.9 És primer?
*/

fun main(){
    val scanner = Scanner(System.`in`)
    println("Introdueix un valor:")
    val number: Int = scanner.nextInt()
    var counter = 0

    for (i in 1..number){
        if (number%i==0)counter++
        if (counter>2)break
    }
    if (counter == 2) println("És primer")
    else println("No és primer")
}