import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 2.3 És un bitllet vàlid
*/

fun main() {

    val scanner = Scanner(System.`in`)
    println("Introdueix €:")
    val diner: Int = scanner.nextInt()

    when (diner) {
        5, 10, 20, 50, 100, 200, 500 -> println(true)
        else -> println(false)
    }
}



