import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 2.4 Té edat per treballar
*/

fun main() {

    val scanner = Scanner(System.`in`)
    println("Introdueix €:")
    val edat: Int = scanner.nextInt()

    if(edat >= 16 && edat <= 65) println("te edat per treballar")
    else println("no te edat per treballar")
}