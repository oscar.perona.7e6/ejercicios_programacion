package `2`

import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 2.10 Calcula la lletra del dni
*/


fun main() {

    val scanner = Scanner(System.`in`)
    println("Introdueix hores, minuts y segons:")
    val number: Int = scanner.nextInt()
    var dni = ""

    when(number%23){
        0 ->  dni = number.toString()+"T"
        1 ->  dni = number.toString()+"R"
        2 ->  dni = number.toString()+"W"
        3 ->  dni = number.toString()+"A"
        4 ->  dni = number.toString()+"G"
        5 ->  dni = number.toString()+"M"
        6 ->  dni = number.toString()+"Y"
        7 ->  dni = number.toString()+"F"
        8 ->  dni = number.toString()+"P"
        9 ->  dni = number.toString()+"D"
        10 ->  dni = number.toString()+"X"
        11 ->  dni = number.toString()+"B"
        12 ->  dni = number.toString()+"N"
        13 ->  dni = number.toString()+"J"
        14 ->  dni = number.toString()+"Z"
        15 ->  dni = number.toString()+"S"
        16 ->  dni = number.toString()+"Q"
        17 ->  dni = number.toString()+"V"
        18 ->  dni = number.toString()+"H"
        19 ->  dni = number.toString()+"L"
        20 ->  dni = number.toString()+"C"
        21 ->  dni = number.toString()+"K"
        22 ->  dni = number.toString()+"E"
    }

    println(dni)
}