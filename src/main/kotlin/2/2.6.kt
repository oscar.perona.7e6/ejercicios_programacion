import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 2.6 Quina pizza és més gran?
*/

fun main(){

    val scanner = Scanner(System.`in`)
    println("Introdueix 3 nombres:")
    val diametre: Int = scanner.nextInt()
    val costat1: Int = scanner.nextInt()
    val costat2: Int = scanner.nextInt()

    val areaRodona = Math.PI*(diametre*diametre)
    val areaRectangular = costat1*costat2

    if(areaRodona > areaRectangular) println("Pizza redona: $areaRodona")
    else println("Pizza rectangular: $areaRectangular")
}