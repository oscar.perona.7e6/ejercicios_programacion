package `2`

import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 2.20 Conversor d’unitats
*/


fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix hores, minuts y segons:")
    val num = scanner.nextInt()
    val unitat: String = scanner.next()

    when (unitat){
        "G" -> {
            println("${num.toDouble()/1000.0} KG")
            println("${num.toDouble()/100000000.0} TN")
        }
        "KG" -> {
            println("${num.toDouble()/1000.0} TN")
            println("${num.toDouble() * 1000.0} G")
        }
        "tn" -> {
            println("${num.toDouble()*1000000000.0} TN")
            println("${num.toDouble() * 1000.0} KG")
        }
    }
}