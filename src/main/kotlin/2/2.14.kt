package `2`

import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 2.10 Calcula la lletra del dni
*/


fun main() {

    val scanner = Scanner(System.`in`)
    println("Introdueix hores, minuts y segons:")
    val year: Int = scanner.nextInt()

    if (year%4 == 0) println("Es un año bisiesto")
    else println("No es un año bisiesto")
}