package `2`

import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 2.18 Valor absolut
*/


fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix hores, minuts y segons:")
    val num: Int = scanner.nextInt()

    if (num<0) println(num*-1)
    else println(num)
}