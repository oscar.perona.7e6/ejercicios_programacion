import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 2.7 Parell o senar?
*/

fun main(){

    val scanner = Scanner(System.`in`)
    println("Introdueix un nombre:")
    val num: Int = scanner.nextInt()

    if(num % 2 == 0) println("parell")
    else println("senar")
}