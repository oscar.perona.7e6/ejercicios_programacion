package `2`

import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 2.9 Canvi mínim
*/


fun main() {

    val scanner = Scanner(System.`in`)
    println("Introdueix hores, minuts y segons:")

    var eur: Int = scanner.nextInt()
    var cent: Int = scanner.nextInt()

    var m1Cent = 0
    var m2Cent = 0
    var m5Cent = 0
    var m10Cent = 0
    var m20Cent = 0
    var m50Cent = 0
    var m1Eur = 0
    var m2Eur = 0

    var b5Eur = 0
    var b10Eur = 0
    var b20Eur = 0
    var b50Eur = 0
    var b100Eur = 0
    var b200Eur = 0
    var b500Eur = 0


    if (eur >= 500){
        b500Eur= eur/500
        eur= eur%500
    }
    if (eur >= 200){
        b200Eur= eur/200
        eur= eur%200
    }
    if (eur >= 100){
        b100Eur= eur/100
        eur= eur%100
    }
    if (eur >= 50){
        b50Eur= eur/50
        eur= eur%50
    }
    if (eur >= 20){
        b20Eur= eur/20
        eur= eur%20
    }
    if (eur >= 10){
        b10Eur= eur/10
        eur= eur%10
    }
    if (eur >= 5){
        b5Eur= eur/5
        eur= eur%5
    }

    if (eur >= 2){
        m2Eur= eur/2
        eur= eur%2
    }

    if (eur >= 1){
        m1Eur= eur/1
        eur= eur%1
    }


    //si hay centimos restantes pasa estos a la variable centimos
    if (eur<1){
        cent= cent+(eur+100)
    }

    if (cent >= 50){
        m50Cent= cent/50
        cent= cent%50
    }
    if (cent >= 20){
        m20Cent= cent/20
        cent= cent%20
    }
    if (cent >= 10){
        m10Cent= cent/10
        cent= cent%10
    }
    if (cent >= 5){
        m5Cent= cent/5
        cent= cent%5
    }
    if (cent >= 2){
        m2Cent= cent/2
        cent= cent%2
    }
    if (cent >= 1){
        m1Cent= cent/1
        cent= cent%1
    }




    println("Bitllets de 500 euros: $b500Eur")
    println("Bitllets de 200 euros: $b200Eur")
    println("Bitllets de 100 euros: $b100Eur")
    println("Bitllets de 50 euros: $b50Eur")
    println("Bitllets de 20 euros: $b20Eur")
    println("Bitllets de 10 euros: $b10Eur")
    println("Bitllets de 5 euros: $b5Eur")

    println("Monedes de 2 euros: $m2Eur")
    println("Monedes de 1 euros: $m1Eur")
    println("Monedes de 50 centimos: $m50Cent")
    println("Monedes de 20 centimos: $m20Cent")
    println("Monedes de 10 centimos: $m10Cent")
    println("Monedes de 5 centimos: $m5Cent")
    println("Monedes de 2 centimos: $m2Cent")
    println("Monedes de 1 centimos: $m1Cent")
}
