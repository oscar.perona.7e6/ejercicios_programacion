package `2`

import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 2.16 Quants dies té el mes
*/


fun main() {

    val scanner = Scanner(System.`in`)
    println("Introdueix un mes:")
    val mes: Int = scanner.nextInt()

    when (mes){
      1, 3, 5, 7, 8, 10, 12  -> println(31)
      4, 6, 9, 11   -> println(30)
      2  -> println(28)
    }
}