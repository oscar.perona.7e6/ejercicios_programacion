/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 2.1 Màxim de 3 nombres enters
*/
import java.util.*


fun main () {

    val scanner = Scanner(System.`in`)
    println("Introdueix tres numeros:")
    val num1: Int = scanner.nextInt()
    val num2: Int = scanner.nextInt()
    val num3: Int = scanner.nextInt()

    if (num1 > num2 && num1 > num3) println("$num1 es el nombre mes gran")
    else if (num2 > num1 && num2 > num3) println("$num2 es el mes gran")
    else if (num3 > num1 && num3 > num2) println("$num3 es el mes gran")

}