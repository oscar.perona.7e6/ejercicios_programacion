import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 2.5 En rang
*/

fun main(){
    val scanner = Scanner(System.`in`)
    println("Introdueix 5 nombres:")
    val num1: Int = scanner.nextInt()
    val num2: Int = scanner.nextInt()
    val num3: Int = scanner.nextInt()
    val num4: Int = scanner.nextInt()
    val num5: Int = scanner.nextInt()

    if(num5 in num1..num2 && num5 in num3..num4) println(true)
    else println(false)
}