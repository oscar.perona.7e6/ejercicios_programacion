package `2`

import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 2.10 Calcula la lletra del dni
*/


fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val char = scanner.next().single()

    when (char){
        'a','e','i','o','u' -> println("Vocal")
        else -> println("Consonante")
    }
}