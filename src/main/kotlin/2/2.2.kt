/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 2.2 Salari
*/

import java.util.*

fun main(){

    val scanner = Scanner(System.`in`)
    println("Introdueix hores treballades:")
    val hores: Int = scanner.nextInt()

    if(hores>40) println(hores*60)
    else println(hores*40)
}