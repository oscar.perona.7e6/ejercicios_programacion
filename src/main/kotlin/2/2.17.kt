package `2`

import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 2.17 Puges o baixes?
*/


fun main() {

    val scanner = Scanner(System.`in`)
    println("Introdueix tres numeros:")
    val num1: Int = scanner.nextInt()
    val num2: Int = scanner.nextInt()
    val num3: Int = scanner.nextInt()

    if (num1>num2&&num2>num3) println("Descendent")
    else if (num1<num2&&num2<num3) println("Ascendent")
    else println("Cap de les dos")
}