package `2`

import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 2.19 Quina nota he tret?
*/


fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix hores, minuts y segons:")
    val nota: Double = scanner.nextDouble()

    when (nota){
        in 0.0 .. 4.9 -> println("Insuficient")
        in 5.0 .. 5.9 -> println("Suficient")
        in 6.0 .. 6.9 -> println("Be")
        in 7.0 .. 8.9 -> println("Notable")
        in 9.0 .. 10.0 -> println("Excelent")
        else -> println("Nota invalida")
    }
}