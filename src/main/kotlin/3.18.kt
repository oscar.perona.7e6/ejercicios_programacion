import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 3.18 Fibonacci
*/

fun main(){
    val scanner = Scanner(System.`in`)
    println("Introdueix una direccio:")
    val n = scanner.nextInt()

    var container = 0
    var container2 = 1

    for (i in 1..n){
        print("$container ")

        val suma = container +container2
        container = container2
        container2 = suma
    }
}
