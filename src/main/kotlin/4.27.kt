import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/11/09
* TITLE: 4.27 Elimina les repeticions
*/


fun main(){

    val scanner = Scanner(System.`in`)
    println("Introduce le tamaño de la lista:")
    var listSize = scanner.nextInt()

    val mutableList: MutableList<String> = mutableListOf()
    val resultList: MutableList<String> = mutableListOf()

    for (i in 1..listSize){
        var listValor = scanner.next()
        mutableList.add(listValor)
    }


    for (i in mutableList.indices){
        if (mutableList[i] !in resultList) resultList.add(mutableList[i])
    }

    println(resultList)


}



