import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 3.16 Coordenades en moviment
*/

fun main(){


    var imput = "b"

    var x =0
    var y =0

    while (imput!="z"){

        val scanner = Scanner(System.`in`)
        println("Introdueix una direccio:")
        var imput = scanner.next()

        if (imput=="n")y--
        if (imput=="s")y++
        if (imput=="e")x++
        if (imput=="o")x--
        if (imput=="z")break


    }

    println("$x, $y")
}