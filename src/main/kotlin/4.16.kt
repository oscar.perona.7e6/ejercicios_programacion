import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/11/09
* TITLE: 4.16 Són iguals?
*/

fun main(){

    val scanner = Scanner(System.`in`)
    println("Introduce dos palabras: ")
    val palabra1 = scanner.next()
    val palabra2 = scanner.next()
    var equals = true


    for (i in palabra1.indices){
            if (palabra1[i] == palabra2[i]) equals = true
            else {
                equals = false
                break
            }
    }

    if (equals==true) println("Si son iguals")
    else println("No son iguals")
}