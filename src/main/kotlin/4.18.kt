import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/11/09
* TITLE: 4.18 Purga de caràcters
*/

fun main(args: Array<String>){

    val scanner = Scanner(System.`in`)
    println("Introdueix un valor:")
    var word = scanner.next()

    do {
        val letter = scanner.next().single()
        if (letter in word) word = word.replace(letter.toString(),"")
    }while (letter != '0')
    println(word)
}