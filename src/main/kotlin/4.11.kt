import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/11/09
* TITLE: 4.11 Quants parells i quants senars?
*/

fun main(args: Array<String>){

    var parells = 0
    var senars = 0

    for (i in args){
        if (i.toInt() % 2 == 0) parells++
        else senars++
    }

    println("Numeros parells: $parells")
    println("Numeros senars: $senars")
}