import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 3.14 Endevina el número
*/

fun main (){

    val number = 1..100
    val randomNumber = number.random()

    var imput = 0

    while (imput!=randomNumber){
        val scanner = Scanner(System.`in`)
        println("Introdueix 1 número:")
        var imput = scanner.nextInt()

        if (imput==randomNumber){
            println("Has encertat!")
            break
        }
        else if (imput<randomNumber) println("Massa petit")
        else if (imput>randomNumber) println("Massa gran")
    }


}