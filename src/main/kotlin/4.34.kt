import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/11/09
* TITLE: 4.34 Suma de matrius
*/

fun main(){

    val scanner = Scanner(System.`in`)
    println("Size of matrix: ")
    val size = scanner.nextInt()


    val firstMatrix: MutableList<MutableList<Int>> = mutableListOf()
    val secondMatrix: MutableList<MutableList<Int>> = mutableListOf()
    val resultMatrix: MutableList<MutableList<Int>> = mutableListOf()


    for (i in 0 until size){
        firstMatrix.add(mutableListOf())
        for (j in 0 until size){
            val number = scanner.nextInt()
            firstMatrix[i].add(number)
        }
    }

    for (i in 0 until size){
        secondMatrix.add(mutableListOf())
        for (j in 0 until size){
            val number = scanner.nextInt()
            secondMatrix[i].add(number)
        }
    }
    for (i in 0 until size){
        resultMatrix.add(mutableListOf())
        for (j in 0 until size){
            resultMatrix[i].add(firstMatrix[i][j] + secondMatrix[i][j])
        }
    }
    for (i in resultMatrix.indices){
    println(resultMatrix[i])
    }
}

