

import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 3.1 Pinta X números
*/

fun main(){

    val scanner = Scanner(System.`in`)
    println("Introdueix un valor:")
    val number: Int = scanner.nextInt()
    var i = 1

    while (i <= number) {
        println(i)
        i++
    }

}