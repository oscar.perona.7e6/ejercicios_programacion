import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/11/09
* TITLE: 4.19 Substitueix el caràcter
*/

fun main(){

    val scanner = Scanner(System.`in`)
    println("Introdueix un valor:")
    var word = scanner.next()
    val leter1 = scanner.next().single()
    val leter2 = scanner.next().single()

    if (leter1 in word){
        word = word.replace(leter1.toString(), leter2.toString())
    }

    println(word)

}