/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 1.11 Calculadora de volum d’aire
*/

import java.util.*

fun main (){

    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix la llargada:")

    val llargada: Double = scanner.nextDouble()
    println("Introdueix l'amplada:")
    val amplada: Double = scanner.nextDouble()
    println("Introdueix l'alçada:")
    val alcada: Double = scanner.nextDouble()

    val area: Double = llargada*amplada*alcada
    println("L'area de la habitacio es: $area")
}