/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 1.17 És edat legal
*/

import java.util.*


fun main () {

        val scanner = Scanner(System.`in`)
        println("Introdueix una edad:")
        val edad: Int = scanner.nextInt()

        val result: Boolean = (edad >= 18)

       println(result)
}