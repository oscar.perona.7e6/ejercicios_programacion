package `1`

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 1.3 Suma de dos nombres enters
*/

import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Introdueix dos números:")
    // Reads an int
    val number1 = scanner.nextInt()
    val number2 = scanner.nextInt()

    println(number1+number2)
}