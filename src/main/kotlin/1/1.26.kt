package `1`

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 1.26 Fes-me minúscula
*/

import java.util.*


fun main () {

    val scanner = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val caracter = scanner.next().single()

    println(caracter.lowercase())
}