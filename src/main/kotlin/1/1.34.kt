package `1`

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 1.34 Hola Usuari
*/

import java.util.*


fun main () {
    val scanner = Scanner(System.`in`)
    println("Introdueix el teu nom:")
    val name = scanner.next()

    println("Bon dia $name")
}