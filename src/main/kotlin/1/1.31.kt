package `1`

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 1.30 Quant de temps?
*/

import java.util.*


fun main () {
    val scanner = Scanner(System.`in`)
    println("Introdueix el nombre de metres:")
    var meter = scanner.nextInt()

    println((meter.toDouble() * 39.37)/12)
}