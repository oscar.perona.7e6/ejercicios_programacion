package `1`

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 1.32 Calcula el capital (no LA capital)
*/

import java.util.*


fun main () {

    val scanner = Scanner(System.`in`)
    println("Introdueix el nombre de metres:")
    var capital = scanner.nextInt()
    val tiempo = scanner.nextInt()
    val porcentaje = scanner.nextInt()

    println((capital+(capital*porcentaje/100))*tiempo.toDouble())

}