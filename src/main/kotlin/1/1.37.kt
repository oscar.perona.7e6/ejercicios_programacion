package `1`

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 1.37 Ajuda per la màquina de viatge en el temps
*/

import java.time.LocalDate
import java.time.ZonedDateTime
import java.util.*


fun main () {
    val date = LocalDate.now()

    println("Avui es $date")
}