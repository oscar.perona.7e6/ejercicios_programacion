package `1`

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 1.21 Tres nombres iguals
*/

import java.util.*


fun main () {
    val scanner = Scanner(System.`in`)
    println("Introdueix tres números:")
    val number1 = scanner.nextInt()
    val number2 = scanner.nextInt()
    val number3 = scanner.nextInt()

    println(number1==number2 && number2==number3)
}