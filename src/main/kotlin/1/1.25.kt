package `1`

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 1.25 Fes-me majúscula
*/

import java.util.*


fun main () {

    val scanner = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val caracter = scanner.next().single()

    println(caracter.uppercase())
}
