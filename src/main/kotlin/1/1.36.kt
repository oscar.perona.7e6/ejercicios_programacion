package `1`

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 1.36 Construeix la història
*/

import java.util.*


fun main () {
    val scanner = Scanner(System.`in`)
    println("Introdueix el teu nom:")
    val line1 = scanner.nextLine()
    val line2 = scanner.nextLine()
    val line3 = scanner.nextLine()

    println("$line1 $line2 $line3")
}