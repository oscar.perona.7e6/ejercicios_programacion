package `1`

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 1.33 És divisible
*/

import java.util.*


fun main () {

    val scanner = Scanner(System.`in`)
    println("Introdueix el nombre de metres:")
    var number1 = scanner.nextInt()
    val number2 = scanner.nextInt()

    println(number1%number2 == 0)
}