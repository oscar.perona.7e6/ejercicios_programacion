package `1`

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 1.4 Calcula l’àrea
*/

import java.util.*

fun main(){

    val scanner = Scanner(System.`in`)
    println("Introdueix amplada y llargada:")
    // Reads an int
    val amplada = scanner.nextInt()
    val llargada = scanner.nextInt()

    println(amplada*llargada)
}