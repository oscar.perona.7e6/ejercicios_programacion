package `1`

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 1.20 Nombres decimals iguals
*/

import java.util.*


fun main () {

    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix dos números decimals:")
    val number1 = scanner.nextDouble()
    val number2 = scanner.nextDouble()

    println(number1 == number2)

}