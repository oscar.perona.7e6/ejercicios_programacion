package `1`

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 1.19 Programa adolescent
*/

import java.util.*


fun main () {

    val scanner = Scanner(System.`in`)
    println("Introdueix un booleà:")
    val userInputValue = scanner.nextBoolean()

    println(!userInputValue)
}