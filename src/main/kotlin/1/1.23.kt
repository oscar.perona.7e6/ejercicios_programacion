package `1`

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 1.23 És una lletra?
*/

import java.util.*


fun main () {
    val scanner = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val caracter = scanner.next().single()

    println(caracter in 'a'..'z' || caracter in 'A'..'Z')
}