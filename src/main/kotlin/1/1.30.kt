package `1`

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 1.30 Quant de temps?
*/

import java.util.*


fun main () {

    val scanner = Scanner(System.`in`)
    println("Introdueix el nombre de segons:")
    var segons = scanner.nextInt()

    var minuts = segons/60
         segons = segons%60

    var hores = minuts/60
         minuts = minuts%60

    println("$hores hora $minuts minuts $segons segons")
}