package `1`

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 1.24 És un nombre?
*/

import java.util.*


fun main () {

    val scanner = Scanner(System.`in`)
    println("Introdueix un caràcter:")
    val number = scanner.next().single()

    println(number in '0'..'9')
}