package `1`

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 1.35 Creador de targetes de treball
*/

import java.util.*


fun main () {

    val scanner = Scanner(System.`in`)
    println("Introdueix el teu nom:")
    val name = scanner.next()
    val lastname = scanner.next()
    val despacho = scanner.nextInt()

    println("Empleada: $name $lastname - Despatx: $despacho")

}