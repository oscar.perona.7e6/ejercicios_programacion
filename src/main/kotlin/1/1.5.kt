package `1`

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 1.5 Operació boja
*/

import java.util.*

fun main(){

    val scanner = Scanner(System.`in`)
    println("Introdueix amplada y llargada:")
    // Reads an int
    val numA = scanner.nextInt()
    val numB = scanner.nextInt()
    val numC = scanner.nextInt()
    val numD = scanner.nextInt()

    println((numA+numB)*(numC%numD))
}