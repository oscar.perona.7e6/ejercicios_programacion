package `1`

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 1.2 Dobla l’enter
*/

import java.util.*

fun main(){
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    // Reads an int
    val number = scanner.nextInt()

    println(number*2)
}