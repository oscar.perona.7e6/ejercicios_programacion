package `1`

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 1.22 Qui riu últim riu millor
*/

import java.util.*


fun main () {
    val scanner = Scanner(System.`in`)
    println("Introdueix tres números:")
    val number1 = scanner.nextInt()
    val number2 = scanner.nextInt()
    val number3 = scanner.nextInt()
    val number4 = scanner.nextInt()
    val number5 = scanner.nextInt()

    println(number5>number1 && number5>number2 && number5>number3 && number5>number4)
}