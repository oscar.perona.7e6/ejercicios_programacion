/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 1.26 Fes-me minúscula
*/

import java.util.*


fun main () {

    val scanner = Scanner(System.`in`)
    println("Introdueix un caràcter en minuscula y un altre en mayuscula:")
    val minuscula = scanner.next().single()
    val mayuscula = scanner.next().single()


    println(minuscula.uppercase() == mayuscula.toString())
}