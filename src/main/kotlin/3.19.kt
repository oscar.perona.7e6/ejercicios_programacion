import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 3.19 Multiplicador de primers
*/

fun main(){
    val scanner = Scanner(System.`in`)
    println("Introdueix una direccio:")
    val number = scanner.nextInt()
    var result = 1L

    for (i in 1..number) {
        var j = 2
        while (j < i && i%j != 0) j++
        if (i == j) result *= i
    }
    println(result)
}