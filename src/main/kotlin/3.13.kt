import java.util.*

/*
* AUTHOR: Oscar Perona Gomez
* DATE: 2022/10/18
* TITLE: 3.13 Logaritme natural de 2
*/

fun main(){
    val scanner = Scanner(System.`in`)
    println("Introdueix 1 número:")
    val n = scanner.nextInt()

    var result:Double =1.0
    var operation = "resta"

    for (i in 2..n){

        var b=i.toDouble()

        if (operation=="resta"){
            result-=1/b
            operation ="suma"
        }
        else{
            result+=1/b
            operation ="resta"
        }

    }
    println(result)
}
