import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 3.2 Calcula la suma dels N primers
*/

fun main (){
    val scanner = Scanner(System.`in`)
    println("Introdueix un valor:")
    val number: Int = scanner.nextInt()

    var i = 0
    var suma=0
    var sumador=0

    while(i<= number){
        suma= suma+i
        i++
    }

    print("suma"+",")
}