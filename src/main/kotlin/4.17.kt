import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/11/09
* TITLE: 4.17 Són iguals? (2)
*/

fun main(){

    val scanner = Scanner(System.`in`)
    println("Introduce dos palabras: ")
    val palabra1 = scanner.next()
    val palabra2 = scanner.next()
    var equals = true


    for (i in palabra1.indices){
        if (palabra1[i].toUpperCase() == palabra2[i].toUpperCase()) equals = true
        else {
            equals = false
            break
        }
    }

    if (equals==true) println("Si son iguals")
    else println("No son iguals")
}