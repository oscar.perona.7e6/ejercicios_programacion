import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 3.17 Factorial!
*/

fun main(){

    val scanner = Scanner(System.`in`)
    println("Introdueix una direccio:")
    var number = scanner.nextInt()

    var factorial =1L
    for (i in 1..number){
        factorial*= i.toLong()
    }
    println(factorial)
}