/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/11/09
* TITLE: 4.24 Inverteix les paraules (2)
*/

import java.util.*
fun main(){
    println("Introduce una frase")
    val scanner = Scanner(System.`in`)
    var words = scanner.nextLine().split(" ")

    for (i in words.lastIndex downTo 0){
        print("${words[i]} ")
    }
}