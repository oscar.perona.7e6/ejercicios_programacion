/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/11/09
* TITLE: 4.21 Parèntesis
*/

import java.util.*
fun main() {
    val scanner = Scanner(System.`in`)
    println("Introdueix una secuencia de parentesis")
    val sequence = scanner.next()
    var result = 0

    for (par in sequence) {
        if (par == '(') result++
        else result--
        if (result < 0)
            break
    }
    if (result != 0) println("no")
    else println("si")
}
