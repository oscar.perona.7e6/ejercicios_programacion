import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 3.7 Revers de l’enter
*/


fun main(){

    val scanner = Scanner(System.`in`)
    println("Introdueix un valor:")
    var number: Int = scanner.nextInt()

    var revers=0

    while (number!=0) {
        var i = number % 10
        revers = revers * 10 + i
        number /= 10
    }

    println(revers)

}