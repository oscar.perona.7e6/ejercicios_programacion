import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 4.8 Igual a l'últim
*/


fun main(args: Array<String>) {

 val last = args.last()
 var equals = 0

    for (i in 0 until args.lastIndex){
        if (args[i] == last) equals++
    }

    println(equals)
}