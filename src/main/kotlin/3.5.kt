import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 3.5 Taula de multiplicar
*/

fun main(){

    val scanner = Scanner(System.`in`)
    println("Introdueix un valor:")
    val number: Int = scanner.nextInt()

    var multiplicador = 1

    while (multiplicador<=10){
        val result= (number*multiplicador)
        println("$number x $multiplicador = $result")
        multiplicador++
    }
}
