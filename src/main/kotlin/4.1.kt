/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 4.1 Suma els valors
*/

fun main(args: Array<String>) {

    var result = 0
    for (i in args){
        result+=i.toInt()
    }

    println(result)
}

