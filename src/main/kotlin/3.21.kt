import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 3.21 Triangle invertit d’*
*/

fun main(){

    val scanner = Scanner(System.`in`)
    println("Introdueix una direccio:")
    var lines = scanner.nextInt()

    for (i in lines downTo   1){
        for (number in 1..i){
            print("*")
        }

        println()
    }
}