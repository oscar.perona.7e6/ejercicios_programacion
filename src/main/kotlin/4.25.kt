/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/11/09
* TITLE: 4.25 Inverteix les paraules (3)
*/

import java.util.*
fun main(){
    val scanner = Scanner(System.`in`)
    var words = scanner.nextLine()

    for (i in words.lastIndex downTo 0){
        print(words[i])
    }
}