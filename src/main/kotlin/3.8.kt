import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 3.8 Nombre de dígits
*/

fun main(){
    val scanner = Scanner(System.`in`)
    println("Introdueix un valor:")
    var number: Int = scanner.nextInt()
    val numberOriginal = number

    var digits=0

    while (number!=0){
        number/=10
        digits++
    }


    println("El numero $numberOriginal te $digits digits")
}