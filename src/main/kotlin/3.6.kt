import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 3.6 Eleva’l
*/

fun main(){

    val scanner = Scanner(System.`in`)
    println("Introdueix un valor:")
    val base: Int = scanner.nextInt()
    var exponent: Int = scanner.nextInt()

    var result: Long = 1

    while (exponent != 0) {
        result *= base.toLong()
        --exponent
    }

    println(result)


}