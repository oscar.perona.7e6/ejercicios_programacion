import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 4.4 És contingut?
*/

fun main(args: Array<String>){

    val imputArray = arrayOf( 1, 2, 3, 4, 5, 6, 7, 8, 9, 10)

    val scanner = Scanner(System.`in`)
    println("Introdueix un valor:")
    val number = scanner.next()

    if (number in args) println("Si esta contingut")
    else println("No esta contingut")
}