import java.util.*

fun main(){

    val scanner = Scanner(System.`in`)
    println("Introdueix 1 número:")
    val n = scanner.nextInt()


    for (i in 1..n){
        if (i%3==0 && i%5!=0) println("$i es divisible per 3")
        else if (i%5==0 && i%3!=0) println("$i es divisible per 5")
        else if (i%3==0 && i%5==0) println("$i es divisible per 3 i per 5")
    }
}