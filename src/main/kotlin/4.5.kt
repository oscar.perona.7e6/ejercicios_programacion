import java.util.*

/*
* AUTHOR: Oscar PErona Gomez
* DATE: 2022/9/21
* TITLE: 4.5 En quina posició?
*/

fun main(args: Array<String>){

    val scanner = Scanner(System.`in`)
    println("Introdueix un valor:")
    var number = scanner.next()

    if (number in args) println(number.toInt()-1)
    else println("No esta contingut")
}